import os

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask_oauthlib.provider import OAuth2Provider
from flask import render_template

from app.utils.import_lib import get_setting


db = SQLAlchemy()
oauth = OAuth2Provider()


def create_app(settings, admin=True):
    from app import models

    app = Flask(
        __name__,
        template_folder=settings.TEMPLATE_FOLDER,
        static_folder=settings.STATIC_FOLDER,
    )
    app.config.from_object(settings)
    
    db.init_app(app)

    oauth.init_app(app)

    from app.validator import MyRequestValidator
    oauth._validator = MyRequestValidator()

    @app.route('/')
    def hello():
        return render_template('index.html')

    return app
