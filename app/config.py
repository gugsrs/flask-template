import os

THIS_PATH = os.path.dirname(__file__)
PROJECT_PATH = os.path.abspath(os.path.join(THIS_PATH, os.pardir))


class Path:
    TEMPLATE_FOLDER = os.path.join(PROJECT_PATH, 'templates')
    STATIC_FOLDER = os.path.join(PROJECT_PATH, 'static')
    MEDIA_FOLDER = os.path.join(PROJECT_PATH, 'media')


class Config(Path):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'this-really-needs-to-be-changed'
    SQLALCHEMY_DATABASE_URI = "postgresql://locahost/app"


class Production(Config):
    DEBUG = False


class Staging(Config):
    DEVELOPMENT = True
    DEBUG = True


class Development(Config):
    DEVELOPMENT = True
    DEBUG = True


class Testing(Config):
    TESTING = True
