from app.components import db, oauth
from app.models import User



class Client(db.Model):
    __tablename__ = 'client'

    client_id = db.Column(db.String(40), primary_key=True)
    client_secret = db.Column(db.String(55), nullable=False)

    user_id = db.Column(db.ForeignKey('users.id'))
    user = db.relationship('User')

    _redirect_uris = db.Column(db.Text)
    _default_scopes = db.Column(db.Text)

    @property
    def client_type(self):
        return 'public'

    @property
    def redirect_uris(self):
        if self._redirect_uris:
            return self._redirect_uris.split()
        return []

    @property
    def default_redirect_uri(self):
        return self.redirect_uris[0]

    @property
    def default_scopes(self):
        if self._default_scopes:
            return self._default_scopes.split()
        return []

    @staticmethod
    def find(id):
        """ Queries the Client table and returns first client with
            matching id.
        :param id: Client id
        """
        return Client.query.filter_by(client_id=id).first()


@oauth.clientgetter
def load_client(client_id):
    client = Client.query.filter_by(client_id=client_id).first()
    print('cliente ', client.client_id)
    return client
