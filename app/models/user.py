from flask_restful import fields

from sqlalchemy import event
from sqlalchemy.orm import backref

from app.components import db
from app.utils import time
from app.utils.models import HasId, Serializable


class User(HasId, Serializable, db.Model):
    """User model."""
    __tablename__ = 'users'

    name = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255))
    avatar = db.Column(db.String(255))

    username = db.Column(db.String(255))
    password = db.Column(db.String(255))

    date = db.Column(
        db.DateTime(timezone=True),
        index=True,
        default=time.utc_now
    )

    uid = db.Column(db.String(255), nullable=False, index=True, unique=True)
    device_token = db.Column(db.String(64), nullable=True)

    structure = {
        'id': fields.Integer,
        'name': fields.String,
        'avatar': fields.String,
    }

    def __repr__(self):
        return '<User: {}>'.format(self.name)

    @staticmethod
    def find_with_password(username, password, *args, **kwargs):
        """ Query the User collection for a record with matching username and
        password hash. If only a username is supplied, find the first matching
        document with that username.
        :param username: Username of the user.
        :param password: Password of the user.
        :param *args: Variable length argument list.
        :param **kwargs: Arbitrary keyword arguments.
        """
        user = User.query.filter_by(username=username).first()
        if user and password:
            return User.query.filter(
                User.username == username,
                User.password == password
            ).first()
        else:
            return user
