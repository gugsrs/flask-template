from functools import wraps

from flask import session, abort, request, g
from flask_restful import reqparse

from app.models import User
from app.components import oauth


def require_auth(method):
    """Require the user to be logged in and have a valid device_token."""
    def require(condition, code):
        if not condition:
            abort(code)

    @oauth.require_oauth()
    @wraps(method)
    def wrapper(self, *args, **kwargs):
        self.user = request.oauth.user
        require(self.user is not None, 401)
        parser = reqparse.RequestParser()
        parser.add_argument('device_token', type=str)
        request_args = parser.parse_args()
        self.device_token = request_args.get('device_token')
        require(self.device_token is not None, 403)
        require(self.device_token == self.user.device_token, 403)
        g.user = self.user 
        return method(self, *args, **kwargs)
    return wrapper
