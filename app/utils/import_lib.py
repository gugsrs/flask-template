import os

from app import config


def get_setting(settings_name=None):
    settings_name = settings_name or os.getenv('SETTINGS', 'Development')
    return getattr(config, settings_name)
