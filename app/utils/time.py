"""Convenience functions for dealing with time."""
import pytz
import datetime

SAO_PAULO_TZ = pytz.timezone('America/Sao_Paulo')

localize = SAO_PAULO_TZ.localize


def utc_now():
    """Returns the current time in Sao Paulo."""
    return localize(datetime.datetime.now())
