#!/usr/bin/env python

import os

from app.wsgi import application
from flask.ext.script import Manager
from flask.ext.migrate import Migrate, MigrateCommand

from app.components import create_app, db


migrate = Migrate(application, db)
manager = Manager(application)

manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
